const TEMPS_VISIBLE = 5000;

let results = [];

function loadTxt(file) {
  const req = new XMLHttpRequest();
  req.open('GET', file, false);
  req.send(null);
  if (req.status === 200 || req.status === 0) {
    results = JSON.parse(req.responseText);
    setContent();
  }
}
loadTxt('LiveSchedule.json');


function setContent(indexAgent) {

  let i = 0;
  let highlight = 1;

  let allHtml = '';

  for (const result of results) {

    const html = `
      <div class="enculay" style="border-left-color:${result.stripecolor};">
        <div class="row${highlight === 0 ? '' : i-1 < 0 || results[i-1].teamorange.score!==' ' && result.teamorange.score === ' ' ? (--highlight,' highlight') : ''}" style="background-image:linear-gradient(to right, rgba(${result.gradientcolor}, 0.85) 0%, rgba(${result.gradientcolor}, 0.85) 50%, rgba(${result.gradientcolor}, 0.0) 100%);">
        <div class="team teamblue" id="teamblue-${i}">
          <div class="name">${result.teamblue.name}</div>
          <div class="logo" style="background-image:url(logos/${result.teamblue.logo})"></div>
          <div class="score">${result.teamblue.score}</div>
        </div>
        <div class="team separator" id="separator-${i}">
          ${result.teamblue.score===' ' ? result.scheduled_time : '-'}
        </div>
        <div class="team teamorange" id="teamorange-${i}">
          <div class="score">${result.teamorange.score}</div>
          <div class="logo" style="background-image:url(logos/${result.teamorange.logo})"></div>
          <div class="name">${result.teamorange.name}</div>
        </div>
        </div>
      </div>
      `;

    allHtml += html;
    i++;
  }

  const container = document.querySelector('.container');
  container.innerHTML = allHtml;
}